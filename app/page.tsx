import Image from 'next/image'
import Link from 'next/link'

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
     Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse quo explicabo voluptate, repellendus odit est laborum quidem doloremque corrupti reiciendis! Architecto qui, eos dolorum dicta similique esse ex earum officiis.

     <Link href='/About'>sf</Link>
    </main>
  )
}
